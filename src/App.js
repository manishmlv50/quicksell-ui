import React, { Component } from 'react';
import './App.css';
import BarChart from 'react-bar-chart';

const margin = {top: 20, right: 20, bottom: 30, left: 50};

class App extends Component {
  constructor(){
    super();
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      date : "",
      category : "",
      type : "cash",
      width: 400,
      amount: 0,
      opt : [],
      expByDate:[
        {text: 'Man', value: 500}, 
        {text: 'Woman', value: 300}],
      expByCategory: [],
      expByType: []
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnChange = this.handleOnChange(this);

  }

  componentDidMount() {
    fetch("http://localhost:3000")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        (error) => {
          alert("error: " +error);
        }
      )

      fetch("http://localhost:3000/categories")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result.data);
          this.setState({
            opt: result.data
          });
        },
        (error) => {
          alert("error: " +error);
        }
      )

      fetch("http://localhost:3000/expByDate")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          var res = [];
          result.forEach((element)=>{
            var label = element._id.day + "/"+element._id.month + "/"+element._id.year;
            var ob = {
              text : label,
              value : element.totalAmount
            }
            res.push(ob);
          
          });
          console.log(res);
          this.setState({
            expByDate : res
          });
        },
        (error) => {
          alert("error: " +error);
        }
      )

      fetch("http://localhost:3000/expByType")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          var res = [];
          result.forEach((element)=>{
            var label = element._id.type;
            var ob = {
              text : label,
              value : element.totalAmount
            }
            res.push(ob);
          
          });
          console.log(res);
          this.setState({
            expByType : res
          });
        },
        (error) => {
          alert("error: " +error);
        }
      )
      fetch("http://localhost:3000/expByCategory")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          var res = [];
          result.forEach((element)=>{
            var label = element._id.category;
            var ob = {
              text : label,
              value : element.totalAmount
            }
            res.push(ob);
          
          });
          console.log(res);
          this.setState({
            expByCategory : res
          });
        },
        (error) => {
          alert("error: " +error);
        }
      )
  }


  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(value);
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.state.date === undefined || this.state.date === ""){
      alert("Please select a date");
      return;
    
    }
    if(this.state.category === "" || this.state.category === "Select"){
      alert("Please select a category")
      return;
    }
    if(this.state.type === ""){
      alert("Please select a type")
      return;
    }
    if(this.state.amount === 0){
      alert("Please enter a valid amount");
      return;
    }

    if(this.state.id === undefined){
      fetch('http://localhost:3000/', {
        method: 'post',
        headers:{
          'content-type': 'application/json'
        },
        body: JSON.stringify({
          date: new Date(this.state.date), 
          amount: this.state.amount, 
          category: this.state.category, 
          type: this.state.type
        })
      }).then(res=>res.json())
        .then(res => {
          console.log(res)
          if(res.error === true){
            alert(res.msg);
            return;
          }

            var items = this.state.items;
            items.push({
              date: new Date(this.state.date).toISOString(), 
              amount: this.state.amount, 
              category: this.state.category, 
              type: this.state.type
            });
            this.setState({
              items : items
            });

            this.forceUpdate();

        });
    }
  
  }

	handleOnChange(value) {
    this.setState({
      category:value
    })
  }
  
  delete(id) {
    return fetch('http://localhost:3000/'+id, {
      method: 'delete'
    }).then(response =>{
      response.json().then(json => {
        if(json.error){
          alert("Unable to delete the record");
          return;
        }

        console.log(this.items);
        var items = this.state.items;

        for (var i = items.length - 1; i >= 0; --i) {
          if (items[i]._id === id) {
            items.splice(i,1);
          }
        }
        this.setState({
          items : items
        })
      });
    });
  }

  update(id) {
    return fetch('http://localhost:3000/'+id, {
      method: 'delete'
    }).then(response =>{
      response.json().then(json => {
        if(json.error)
          alert("Unable to delete the record");

        //Remove object form the list
        
      });
    });
  }

  render() {

    const { error, isLoaded, opt,items,expByDate, expByCategory,expByType } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="App">
          <header className="App-header">
          <div style={{width: '33%', float: 'left'}}> 
                <BarChart ylabel='Expense By Date'
                  width={this.state.width}
                  height={280}
                  margin={margin}
                  data={expByDate}/>
            </div>
            <div style={{width: '33%', float: 'left'}}> 
                <BarChart ylabel='Expense By Category'
                  width={this.state.width}
                  height={280}
                  margin={margin}
                  data={expByCategory}/>
            </div>
            <div style={{width: '33%', float: 'left'}}> 
                <BarChart ylabel='Expense By Type'
                  width={this.state.width}
                  height={280}
                  margin={margin}
                  data={expByType}/>
            </div>
          </header>
          

      <div className="container">
        <h2>Add New Expense</h2>
        <form className="form" onSubmit={this.handleSubmit}>
          <label>
            Date: 
            <input id="date-picker" ref="date-picker" type="date" name="date" value={this.state.date} onChange={this.handleInputChange} />
          </label>
          <label>
            Category: 
            <select value={this.state.category} name="category" onChange={this.handleInputChange}>
            <option value="Select">Select a category</option>
            {opt.map((exp, i) => <SelectOption key = {i} 
                      data = {exp} />)}
            </select>
          </label>
          <label>
            Type: <input  type="radio" name="type" value="cash" checked={this.state.type === 'cash'}  onChange={this.handleInputChange} /> Cash
            <input type="radio" name="type" value="credit" checked={this.state.type === 'credit'}  onChange={this.handleInputChange} /> Credit
          </label>
          <label>
            Amount: <input required type="number" name="amount" value={this.state.amount} onChange={this.handleInputChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>

          <table className="grid-table">
            <thead>
              <tr>
                <td>Date</td>
                <td>Category</td>
                <td>Type</td>
                <td>Amount</td>
                <td>Action</td>
              </tr>
              </thead>
              <tbody>
                {items.map((exp, i) => <TableRow key = {i} 
                    data = {exp} onDelete={this.delete} onUpdate={this.update} />)}
              </tbody>
          </table>
        </div>
      );
    }
  }
}


//Table Row
class TableRow extends React.Component {
  delete(id){
    this.props.onDelete(id);
  }
  render() {
     return (
        <tr>
           <td>{this.props.data.date}</td>
           <td>{this.props.data.category}</td>
           <td>{this.props.data.type}</td>
           <td>{this.props.data.amount}</td>
           <td><button onClick={()=>{this.delete(this.props.data._id)}}>Delete</button> </td>
        </tr>
     );
  }
}


//Table Row
class SelectOption extends React.Component {
  
  render() {
     return (
        <option value={this.props.data}>{this.props.data}</option>
     );
  }
}


export default App;
